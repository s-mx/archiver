#include "Huffman.h"

#include <istream>
#include <ostream>

using namespace Arch;

enum Error {
    OK = 0,
    WRONG_ARGUMENTS,
    // You can add here yours errors.
};

int Arch::compress(std::istream *in, std::ostream *out) {
    if (in == nullptr || out == nullptr) {
        return Error::WRONG_ARGUMENTS;
    }

    return Error::OK;
}

int Arch::decompress(std::istream *in, std::ostream *out) {
    if (in == nullptr || out == nullptr) {
        return Error::WRONG_ARGUMENTS;
    }

    return Error::OK;
}
